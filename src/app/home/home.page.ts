import { ApiService } from './../services/api.service';
import { Component } from '@angular/core';
import { Geolocation } from '@ionic-native/geolocation/ngx';

let lat = 0, lon = 0

@Component({
  selector: 'app-home',
  templateUrl: 'home.page.html',
  styleUrls: ['home.page.scss'],
})
export class HomePage {
  city: string = '';
  weather: Array<any> = new Array();
  temp: Array<any> = new Array();
  main: Array<any> = new Array();
  resp: Array<any> = new Array();
  
  constructor(private apiService: ApiService, private geolocation: Geolocation) {
    
  }

  ngOnInit() {
    this.geolocation.getCurrentPosition().then((resp) => {
      lat = resp.coords.latitude
      lon = resp.coords.longitude
      this.getWeatherByCoords()
    }).catch((error) => {
      console.log('Error getting location', error);
    })
  }

  getWeatherByName() {
    var city = this.city
    this.apiService.getWeatherByName(city).subscribe(weather => {
      this.weather = weather;
      this.temp = weather.main;
    })
  }

  getWeatherByCoords() {
    this.apiService.getWeatherByCoords(lat, lon).subscribe(weather => {
      this.weather = weather;
      this.temp = weather.main;
    })
  }
}

