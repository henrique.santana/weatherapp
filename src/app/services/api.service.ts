import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { Geolocation } from '@ionic-native/geolocation/ngx';

@Injectable({
  providedIn: 'root'
})
export class ApiService {
  private apiKey: string = '97aca6ca0a7b669dd8d29741fdc9e16a';
  private api: string = 'https://api.openweathermap.org/data/2.5/';
  constructor(private http: HttpClient, private geolocation: Geolocation) { }

getWeatherByCoords(lat, lon) : Observable<any>{
  return this.http.get(`${this.api}weather?lat=${lat}&lon=${lon}&appid=${this.apiKey}`)
}

getWeatherByName(city) : Observable<any>{
  return this.http.get(`${this.api}weather?q=${city}&appid=${this.apiKey}`);
}

async getPosition() {
  return await this.geolocation.getCurrentPosition().then((resp) => {
  }).catch((error) => {
    console.log('Error getting location', error);
  }) 
}
}